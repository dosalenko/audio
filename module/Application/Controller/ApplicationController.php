<?php

namespace Application\Controller;

use Core\SystemController;
use Audio\Controller\AudioController;

class ApplicationController extends SystemController
{
    /**
     * Index Action controller
     */
    public function indexAction()
    {
        $this->config['module'] = 'Audio';
        $page = new AudioController($this->config);
        $page->listAction();
        return;
    }
}