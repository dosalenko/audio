<?php

namespace Application\Model;

use Core\SystemModel;

/**
 * Concrete class for audio
 * Business layer for audio object.
 */
class AudioEntity extends SystemModel
{
    /**
     * Id
     * @var integer $id
     */
    public $id;

    /**
     * label
     * @var string $label
     */
    public $label;

    /**
     * album_title
     * @var string $album_title
     */
    public $album_title;

    /**
     * $artist_title
     * @var string $artist_title
     */
    public $artist_title;

    /**
     * year
     * @var string $year
     */
    public $year;

    /**
     * cost
     * @var string $cost
     */
    public $cost;

    /**
     * storage_code
     * @var $storage_code
     */
    public $storage_code;




}