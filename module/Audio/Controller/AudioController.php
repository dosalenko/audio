<?php

namespace Audio\Controller;

use Core\SystemController;
use Audio\Model\Audio;
use Application\Controller\ErrorController;

/**
 * Controller for audio pages
 */
class AudioController extends SystemController
{
    /**
     * Index audios page
     */
    public function indexAction()
    {
        $this->listAction();
    }

    /**
     * List all audios including sorting
     */
    public function listAction()
    {
        $audio = new Audio($this->config);
        $audios = $audio->getAll();
        $amountOfAudios = $audio->getAmountAudios();
        
        // load views.
        $this->view('audio/list', [
            'audios'         => $audios,
            'amountOfAudios' => $amountOfAudios
        ]);
    }

    /**
     * Add audio action
     */
    public function addAction()
    {
        if (isset($_POST['submit_add_audio'])) {
            $audio = new Audio($this->config);
            $audio->addAudio([
              
                
    
    `album_title`       => $_POST['album_title'],
    `artist_title`      => $_POST['artist_title'], 
    `year`              => $_POST['year'],
    `purchase_date`     => 123,
    `cost`              => $_POST['cost'],
    `storage_code`      => $_POST['storage_code'],
                
                
                
                
            ]);
            header('location: ' . $this->config['base_path']);
        } else {
            // load views.
            $this->view('audio/add');
        }
    }

    /**
     * Edit audio action
     * @param string $audioId Id of the to edit audio
     */
    public function editAction($audioId)
    {
        
            if (isset($_POST['submit_update_audio'])) {
                $audio = new Audio($this->config);
                $audio->updateAudio([
                    'audioId'   => $_POST['id'],
                    'album_title' => $_POST['album_title'],
                    'artist_title' => $_POST['artist_title'],

                ]);
                header('location: ' . $this->config['base_path']);
            } elseif (isset($audioId)) {
                $audio = new Audio($this->config);
                $audio = $audio->getAudio($audioId);
    
                if ($audio === false) {
                    $page= new ErrorController($this->config);
                    $page->errorAction();
                } else {
                    // load views.
                    $this->view('audio/edit', [
                        'audio' => $audio
                    ]);
                }
            } else {
                header('location: ' . $this->config['base_path']);
            }
            
    }


    public function removeAction($audioId)
    {

       //remove audio
        header('location: ' . $this->config['base_path']);


    }

}