<?php

namespace Audio\Model;

use Application\Model\AudioEntity;
use Core\Upload;

class Audio extends AudioEntity
{
    /**
     * Get all audios
     */
    public function getAll()
    {
        $query = $this->db->prepare('SELECT * FROM audio');
        $query->execute();
        
        return $query->fetchAll();
    }

    /**
     * Get amount audios
     */
    public function getAmountAudios()
    {
        $query = $this->db->prepare('SELECT COUNT(id) AS amount_of_audios FROM audio');
        $query->execute();

        return $query->fetch()->amount_of_audios;
    }

    /**
     * Get audio from database
     * @param integer $audioId
     * @return object $db
     */
    public function getAudio($audioId)
    {
        $query = $this->db->prepare('SELECT * FROM audio WHERE id = :id LIMIT 1');
        $query->execute(array(':id' => $audioId));
        
        return ($query->rowcount() ? $query->fetch() : false);
    }

    /**
     * Add an audio to database
     * @param array $data Data for add audio
     */
    public function addAudio(array $data)
    {
        extract($data);
        $sql = 'INSERT INTO audio (album_title, artist_title, year, purchase_date, cost, storage_code) VALUES (:album_title, :artist_title, :year, :purchase_date, :cost, :storage_code)';
        $query = $this->db->prepare($sql);

        $query->execute(array(
            ':album_title' => $_POST['album_title'],
            ':artist_title'    => $_POST['artist_title'],
            ':year' => $_POST['year'],
            ':purchase_date' => $_POST['purchase_date'],
            ':cost' => $_POST['cost'],
            ':storage_code' => $_POST['storage_code']
        ));

        $handle = new Upload($_FILES['label']['tmp_name']);
        if ($handle->uploaded) {
            $handle->file_new_name_body   = 'image_' . $this->db->lastInsertId();
            $handle->image_resize         = true;
            $handle->image_x              = 320;
            $handle->image_ratio_y        = true;
            $handle->allowed= [
                'image/jpeg',
                'image/jpeg',
                'image/jpeg',
                'image/gif',
                'image/png'
            ];

            $handle->process($_SERVER['DOCUMENT_ROOT'] . '/img/');
            if ($handle->processed) {
                $handle->clean();

                $query = $this->db->prepare('UPDATE `audio` SET label = :label WHERE id = :id');
                $query->execute(array(
                    ':id'     => $this->db->lastInsertId(),
                    ':label'  => $handle->file_dst_name
                ));
            }
        }
    }

    /**
     * Update an audio in database
     * @param array $data Data for update audio
     */
    public function updateAudio(array $data)
    {
        extract($data);
        $query = $this->db->prepare('UPDATE audio SET album_title = :album_title, artist_title = :artist_title WHERE id = :id');

        $query->execute(array (
            ':album_title' => $_POST['album_title'],
            ':artist_title' => $_POST['$artist_title'],

        ));
    }
}