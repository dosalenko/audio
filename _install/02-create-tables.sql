CREATE TABLE IF NOT EXISTS `audio` (
    `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `label` varchar(255) NOT NULL DEFAULT '',
    `album_title` varchar(255) NOT NULL DEFAULT '',
    `artist_title` varchar(255) NOT NULL DEFAULT '',
    `year` int(4) NOT NULL DEFAULT 0,
    `purchase_date` varchar(255) NOT NULL DEFAULT 0,
    `cost` float NOT NULL,
    `storage_code` varchar(255) NOT NULL DEFAULT ''

) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
