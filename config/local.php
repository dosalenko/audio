﻿<?php

/**
 * Configuration file
 */
return array(
    /** Project Name */
    'project_name' => 'audioCD',

    /** audio base path */
    'base_path' => 'http://audionew.loc/',

    /** audio DB settings */
    'audio' => array(
        /** connection settings */
        'driver'   => 'Pdo',
        'dsn'      => 'mysql:host=localhost;dbname=audio;charset=utf8mb4',
        'username' => 'root',
        'password' => '2989',
        'options'  => array(
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING
        ),
    ),

    /** audio modules */
    'modules' => array(
        'Application',
        'Audio',
    ),
);